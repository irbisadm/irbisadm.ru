module.exports = function (grunt) {

    grunt.initConfig({
        "babel": {
            options: {
                sourceMap: true
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: './src/js/',
                    src: "**/*.js",
                    ext: '.js',
                    dest: "./tmp/js/"
                }]
            }
        },
        postcss: {
            options: {
                map: {
                    inline: false,
                    annotation: 'dist/assets/map/'
                },
                processors: [
                    require('postcss-custom-properties'),
                    require('postcss-nested'),
                    require('autoprefixer')({browsers: 'last 3 versions'}),
                    require('cssnano')()
                ]
            },
            dist: {
                src: ['tmp/css/build.raw.css'],
                dest: 'dist/assets/css/style.css'
            }
        },
        pug: {
            compile: {
                options: {
                    pretty: true
                },
                files: [{
                    expand: true,
                    cwd: './src/pages/',
                    src: "**/*.pug",
                    ext: '.html',
                    dest: "./dist/"
                }]
            }
        },
        watch: {
            pug: {
                files: ['src/pages/*.pug', 'src/pages/**/*.pug', 'src/templates/*.pug'],
                tasks: ['pug'],
                options: {
                    spawn: false
                }
            },
            postcss: {
                files: ['src/css/*.pcss'],
                tasks: ['concat', 'postcss'],
                options: {
                    spawn: false
                }
            },
            js: {
                files: ['src/js/*.js'],
                tasks: ['babel','concat:js'],
                options: {
                    spawn: false
                }
            },
            img: {
                files: ['src/img/*.png', 'src/img/*.jpg', 'src/img/*.gif'],
                tasks: ['newer:imagemin'],
                options: {
                    spawn: false
                }
            }
        },
        concat: {
            css: {
                src: ['src/css/*.pcss'],
                dest: 'tmp/css/build.raw.css'
            },
            js: {
                src: ['tmp/js/*.js'],
                dest: 'tmp/pack.js'
            }
        },
        uglify: {
            my_target: {
                files: {
                    'dist/assets/js/pack.min.js': ['tmp/pack.js']
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'src/img',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'dist/assets/img/'
                }]
            }
        },
        copy: {
            svg: {
                files: [
                    {expand: true, flatten: true, src: ['src/svg/**'], dest: 'dist/assets/svg/', filter: 'isFile'}
                ]
            }
        }
    });


    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-pug');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-newer');

    grunt.registerTask('build', ['imagemin', 'concat','pug', 'babel', 'postcss','copy']);

    grunt.registerTask("default", ['build',"watch"]);
}