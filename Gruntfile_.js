module.exports = function (grunt) {
  grunt.initConfig({
    postcss: {
      options: {
        map: {
          inline: false,
          annotation: 'assets/map/'
        },
        processors: [
          require('pixrem')(),
          require('autoprefixer')({browsers: 'last 3 versions'}),
          require('cssnano')()
        ]
      },
      dist: {
        src: ['assets/css/style.raw.css'],
        dest: 'assets/css/style.css'
      }
    },
    jade: {
      compile: {
        options: {
          pretty: true
        },
        files: [{
          expand: true,
          cwd: './src/pages/pages/',
          src: "**/*.jade",
          ext: '.html'
        }]
      }
    },
    watch: {
      jade: {
        files: ['src/pages/*.jade', 'src/pages/**/*.jade'],
        tasks: ['jade'],
        options: {
          spawn: false
        }
      },
      postcss: {
        files: ['src/css/*.css'],
        tasks: ['concat','postcss'],
        options: {
          spawn: false
        }
      },
      img: {
        files: ['src/img/*.png', 'src/img/*.jpg', 'src/img/*.gif'],
        tasks: ['newer:imagemin'],
        options: {
          spawn: false
        }
      },
      js: {
        files: ['src/js/*.js'],
        tasks: ['concat','uglify'],
        options: {
          spawn: false
        }
      }
    },
    concat: {
      js: {
        src: ['src/js/*.js'],
        dest: 'assets/js/pack.js'
      },
      css:{
        src:['src/css/*.css'],
        dest:'assets/css/style.raw.css'
      }
    },
    uglify: {
      my_target: {
        files: {
          'assets/js/pack.min.js': ['assets/js/pack.js']
        }
      }
    },
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'src/img',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'assets/img/'
        }]
      }
    }
  });
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-pages');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-newer');

  grunt.registerTask('default', ['newer:imagemin', 'jade', 'concat','postcss', 'uglify', 'watch']);
  grunt.registerTask('publish', ['newer:imagemin', 'jade', 'concat','postcss', 'uglify']);
}

"autoprefixer-core": "^6.0.1",
    "babel-core": "^6.24.0",
    "babel-preset-env": "^1.3.2",
    "cssnano": "^3.10.0",
    "grunt": "^1.0.1",
    "grunt-babel": "^6.0.0",
    "grunt-contrib-concat": "^1.0.1",
    "grunt-contrib-copy": "^1.0.0",
    "grunt-contrib-pages": "^1.0.0",
    "grunt-contrib-watch": "^1.0.0",
    "grunt-postcss": "^0.8.0",
    "load-grunt-tasks": "^3.5.2",
    "postcss": "^5.2.16",
    "postcss-custom-properties": "^5.0.2",
    "postcss-nested": "^1.0.0",
    "postcss-simple-vars": "^3.1.0",
    "reduce-css-calc": "^1.3.0"